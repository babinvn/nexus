# Nexus setup with docker

The goal is to create a docker repository using Nexus that allows anonymous pull
for `public` images and provides access to other images to authenticated users.

## Hetzner machine

I use Hetzner to run Nexus repository. This setup is a
CX21-machine, though Nexus would prefer a 4-CPU config.

```
# docker-machine rm nexus
docker-machine create \
--driver=hetzner \
--hetzner-api-token=$HETZNER_TOKEN \
--hetzner-server-type=cx21 \
--hetzner-server-location=fsn1 \
nexus
```

This setup uses `intranet` (local) and `internet` (public) networks:

```
docker-machine ssh nexus

docker network create -d bridge \
-o com.docker.network.bridge.host_binding_ipv4=$(ifconfig eth0 | grep "inet " | awk '{print $2}') \
internet

docker network create -d bridge \
-o com.docker.network.bridge.host_binding_ipv4=$(ifconfig ens10 | grep "inet " | awk '{print $2}') \
intranet
```

Upgrade the server:

```
docker-machine ssh nexus
apt-get update
apt-get dist-upgrade
```
or
```
do-release-upgrade
```
then reboot.

## Nginx

This setup uses Nginx as a reverse proxy to your Nexus repository.

Create SSL certificate, replace `example.com` with your domain name:

```
docker run --rm -it \
-v /var/docker/letsencrypt:/etc/letsencrypt \
-p 80:80 certbot/certbot \
certonly --standalone -d nexus.example.com \
-m admin@example.com
```

Copy Nginx configuration files to the Hetzner machine:

```
docker-machine scp nginx/default.conf nexus:/var/docker/nginx/conf.d/
docker-machine scp nginx/nexus.conf nexus:/var/docker/nginx/conf.d/
```

Test your Nginx configuration:

```
docker run -it --rm \
-v /var/docker/nginx/conf.d:/etc/nginx/conf.d \
nginx:latest nginx -t
```

## Nexus

Create folders for metadata and data volumes:

```
mkdir -p /var/docker/nexus/root /var/docker/nexus/data
chown -R 200:200 /var/docker/nexus/*
```

Then start your docker containers:

```
docker-compose up -d
```

Login to Nexus admin console and create `docker` repository:

![Screenshot](images/Screenshot-01.png)
![Screenshot](images/Screenshot-02.png)

Create content selector:

![Screenshot](images/Screenshot-03.png)

Create a privilege based on the content selector:

![Screenshot](images/Screenshot-04.png)

Create a custom anonymous user role with the privilege:

![Screenshot](images/Screenshot-05.png)

Assign the role to anonymous user:

![Screenshot](images/Screenshot-06.png)

Enable anonymous user for your Nexus instance:

![Screenshot](images/Screenshot-07.png)

Create a standard user role with `nx-repository-view-docker-docker-*` privilege
to push images and you are done.

## Test

Test your setup:

```
$ docker login nexus.example.com
Login Succeeded

$ docker push nexus.example.com/public/my-demo-image
Using default tag: latest
The push refers to repository [nexus.example.com/public/my-demo-image]
04685cc23af8: Pushed
482478b02102: Pushed
b1e996f35be4: Pushed
209948787b21: Pushed
2d48c08a6ab4: Pushed
f70efe114ba4: Pushed
1251204ef8fc: Pushed
47ef83afae74: Pushed
df54c846128d: Pushed
be96a3f634de: Pushed

$ docker tag nexus.example.com/public/my-demo-image nexus.example.com/private/my-demo-image

$ docker push nexus.example.com/private/my-demo-image
Using default tag: latest
The push refers to repository [nexus.example.com/private/my-demo-image]
04685cc23af8: Layer already exists
482478b02102: Layer already exists
b1e996f35be4: Layer already exists
209948787b21: Layer already exists
2d48c08a6ab4: Layer already exists
f70efe114ba4: Layer already exists
1251204ef8fc: Layer already exists
47ef83afae74: Layer already exists
df54c846128d: Layer already exists
be96a3f634de: Layer already exists
```

Then logout and test as anonymous user:

```
$ docker logout nexus.example.com
Removing login credentials for nexus.example.com

$ docker pull nexus.example.com/private/my-demo-image
Using default tag: latest
Error response from daemon: unauthorized: access to the requested resource is not authorized

$ docker pull nexus.example.com/public/my-demo-image
Using default tag: latest
latest: Pulling from public/my-demo-image
Digest: sha256:...
Status: Image is up to date for nexus.example.com/public/my-demo-image:latest
```
